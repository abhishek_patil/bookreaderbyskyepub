package com.example.abhishek.bookreaderwithskyepub.utils;

/**
 * Created by Abhishek on 10/8/2016.
 */

public class Color {

    //final colors
    public static final int YELLOW = 0x66FFFF00;
    public static final int GREEN = 0x6600FF00;
    public static final int RED = 0x66FF3232;
    public static final int BLUE = 0x660080FF;

}
