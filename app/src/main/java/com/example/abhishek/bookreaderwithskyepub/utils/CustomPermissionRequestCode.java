package com.example.abhishek.bookreaderwithskyepub.utils;

/**
 * Created by Abhishek on 10/8/2016.
 */

public class CustomPermissionRequestCode {

    public static int INTERNET_PERMISSION = 1;
    public static int READ_EXT_STORAGE = 2;
    public static int BLUETOOTH = 3;

}
