package com.example.abhishek.bookreaderwithskyepub.dto;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Abhishek on 10/15/2016.
 */

public class Bookmark implements Parcelable{

    int bookCode;
    int code;
    int chapterIndex;
    int pageIndex;
    double pagePosition;
    String bookmarkPageInfo;

    public Bookmark() {
    }

    protected Bookmark(Parcel in) {
        bookCode = in.readInt();
        code = in.readInt();
        chapterIndex = in.readInt();
        pageIndex = in.readInt();
        pagePosition = in.readDouble();
        bookmarkPageInfo = in.readString();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(bookCode);
        dest.writeInt(code);
        dest.writeInt(chapterIndex);
        dest.writeInt(pageIndex);
        dest.writeDouble(pagePosition);
        dest.writeString(bookmarkPageInfo);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Bookmark> CREATOR = new Creator<Bookmark>() {
        @Override
        public Bookmark createFromParcel(Parcel in) {
            return new Bookmark(in);
        }

        @Override
        public Bookmark[] newArray(int size) {
            return new Bookmark[size];
        }
    };

    public int getBookCode() {
        return bookCode;
    }

    public void setBookCode(int bookCode) {
        this.bookCode = bookCode;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public int getChapterIndex() {
        return chapterIndex;
    }

    public void setChapterIndex(int chapterIndex) {
        this.chapterIndex = chapterIndex;
    }

    public int getPageIndex() {
        return pageIndex;
    }

    public void setPageIndex(int pageIndex) {
        this.pageIndex = pageIndex;
    }

    public double getPagePosition() {
        return pagePosition;
    }

    public void setPagePosition(double pagePosition) {
        this.pagePosition = pagePosition;
    }

    public String getBookmarkPageInfo() {
        return bookmarkPageInfo;
    }

    public void setBookmarkPageInfo(String bookmarkPageInfo) {
        this.bookmarkPageInfo = bookmarkPageInfo;
    }
}
