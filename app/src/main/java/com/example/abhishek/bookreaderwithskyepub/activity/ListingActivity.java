package com.example.abhishek.bookreaderwithskyepub.activity;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Environment;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.example.abhishek.bookreaderwithskyepub.utils.CustomPermissionRequestCode;
import com.example.abhishek.bookreaderwithskyepub.R;
import com.example.abhishek.bookreaderwithskyepub.dto.FileObject;

import java.io.File;
import java.util.ArrayList;

public class ListingActivity extends AppCompatActivity {

    File externalRootFile;

    private ArrayList<File> fileList;
    private ArrayList<FileObject> fileObjects;

    private ProgressDialog progressDialog;

    private Button tryAgainButton;

    private RelativeLayout noFilesFoundRelativeLayout;

    private ListView filesListView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing);

        initialiseView();

    }

    private void initialiseView() {

        checkRequiredPermissions();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        externalRootFile = new File(Environment.getExternalStorageDirectory().getAbsolutePath());
        progressDialog = new ProgressDialog(this);
        fileList = new ArrayList<>();
        fileObjects = new ArrayList<>();


        tryAgainButton = (Button) findViewById(R.id.tryAgainButton);
        noFilesFoundRelativeLayout = (RelativeLayout) findViewById(R.id.noFilesFoundRelativeLayout);
        filesListView = (ListView) findViewById(R.id.filesListView);

        noFilesFoundRelativeLayout.setVisibility(View.INVISIBLE);

        tryAgainButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getEpubFiles();
            }
        });

        getEpubFiles();

    }

    public void getEpubFiles() {

        class GetFileList extends AsyncTask<Void, Void, Boolean> {


            @Override
            protected void onPreExecute() {
                super.onPreExecute();

                showProgressDialog("Searching EPUB File", "Please Wait...", false);

            }

            @Override
            protected Boolean doInBackground(Void... params) {

                ArrayList<File> files = getAllEpubFileList(externalRootFile);

                if(files.size() == 0) return false;

                return true;
            }


            @Override
            protected void onPostExecute(Boolean aBoolean) {
                super.onPostExecute(aBoolean);

                hideProgressDialog();

                if (aBoolean) {

                    setArrayAdapter(fileList);

                } else {

                    showNoFileFoundRelativeLayout();

                }

            }
        }

        GetFileList getFileList = new GetFileList();
        getFileList.execute();


    }

    private void setArrayAdapter(ArrayList<File> fileList) {
        ArrayList<String> fileNamesArrayList = new ArrayList<>();

        for (File file : fileList) {
            FileObject fileObject = new FileObject(file);

            fileObjects.add(fileObject);

            fileNamesArrayList.add(file.getName());
        }

        ArrayAdapter<String> fileNamesArrayAdapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, fileNamesArrayList);

        filesListView.setAdapter(fileNamesArrayAdapter);

        filesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                goToBookReadActivity(fileObjects.get(position));
            }
        });
    }

    public ArrayList<File> getAllEpubFileList(File dir) {
        File listFile[] = dir.listFiles();
        if (listFile != null && listFile.length > 0) {
            for (int i = 0; i < listFile.length; i++) {
                if (listFile[i].isDirectory()) {
                    getAllEpubFileList(listFile[i]);
                } else {
                    if (listFile[i].getName().endsWith(".epub")) {
                        fileList.add(listFile[i]);
                    }
                }

            }
        }
        return fileList;
    }

    public void toggleNoFileFoundRelativeLayout() {

        if(noFilesFoundRelativeLayout.getVisibility() == View.VISIBLE) {
            noFilesFoundRelativeLayout.setVisibility(View.VISIBLE);
        } else {
            noFilesFoundRelativeLayout.setVisibility(View.INVISIBLE);
        }
    }

    public void showNoFileFoundRelativeLayout() {
        noFilesFoundRelativeLayout.setVisibility(View.VISIBLE);
    }

    public void hideNoFileFoundRelativeLayout() {
        noFilesFoundRelativeLayout.setVisibility(View.INVISIBLE);
    }

    public void goToBookReadActivity(FileObject fileObject) {

        Intent bookReadIntent = new Intent(this, BookReadActivity.class);
        bookReadIntent.putExtra("SELECTED_FILE", fileObject);
        startActivity(bookReadIntent);

    }

    private void showProgressDialog(String title, String message, boolean cancelable) {
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(cancelable);
        progressDialog.show();
    }

    private void hideProgressDialog() {
        if (progressDialog.isShowing()) progressDialog.dismiss();
    }

    public void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    public void showLog(String tag, String value) {

        //this is because tag for log should be less than 23 characters

        if(tag.length() > 22) {
            tag = tag.substring(0, 22);
        }

        Log.d(tag, value);
    }

    private void checkRequiredPermissions() {
        //checking self permissions
        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.INTERNET) != PackageManager.PERMISSION_GRANTED) {
            //request for Internet permission
            requestInternetPermission();
        }

        if(ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            //request for ReadExternal Storage
            requestReadExternalStoragePermission();
        }

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.BLUETOOTH) != PackageManager.PERMISSION_GRANTED) {
            //request for Write External Storage
            requestBlueToothPermission();
        }
    }

    private void requestInternetPermission() {
        if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.INTERNET)) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.INTERNET}, CustomPermissionRequestCode.INTERNET_PERMISSION);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.INTERNET}, CustomPermissionRequestCode.INTERNET_PERMISSION);
        }
    }

    private void requestReadExternalStoragePermission() {
        if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.READ_EXTERNAL_STORAGE}, CustomPermissionRequestCode.READ_EXT_STORAGE);
        } else {
            ActivityCompat.requestPermissions(this, new String[] {Manifest.permission.READ_EXTERNAL_STORAGE}, CustomPermissionRequestCode.READ_EXT_STORAGE);
        }
    }

    private void requestBlueToothPermission() {
        if(ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.BLUETOOTH)) {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.BLUETOOTH}, CustomPermissionRequestCode.BLUETOOTH);
        } else {
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.BLUETOOTH}, CustomPermissionRequestCode.BLUETOOTH);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == CustomPermissionRequestCode.INTERNET_PERMISSION) {
            if(grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                showLog("Internet Permission", "Added");
            } else {
                showLog("Internet Permission", "Not Added");
            }
            requestReadExternalStoragePermission();
        } else if(requestCode == CustomPermissionRequestCode.READ_EXT_STORAGE){
            if(grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                showLog("Read ext Permission", "Added");
            } else {
                showLog("Read ext Permission", "Not Added");
            }
            initialiseView();
        } else if (requestCode == CustomPermissionRequestCode.BLUETOOTH){
            if(grantResults.length == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                showLog("bluetooth permission", "Added");
            } else {
                showLog("bluetooth permission", "Not Added");
                finishActivity();
            }
            initialiseView();
        }else {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void finishActivity() {
        this.finish();
    }

}
