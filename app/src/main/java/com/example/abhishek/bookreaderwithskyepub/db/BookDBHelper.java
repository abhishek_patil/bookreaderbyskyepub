package com.example.abhishek.bookreaderwithskyepub.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseErrorHandler;
import android.database.DatabaseUtils;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.abhishek.bookreaderwithskyepub.dto.Bookmark;
import com.example.abhishek.bookreaderwithskyepub.dto.FileObject;
import com.example.abhishek.bookreaderwithskyepub.dto.PagePosition;
import com.skytree.epub.Book;
import com.skytree.epub.Highlight;
import com.skytree.epub.Highlights;
import com.skytree.epub.PageInformation;

import java.util.ArrayList;

/**
 * Created by Abhishek on 10/8/2016.
 */

public class BookDBHelper extends SQLiteOpenHelper {

    public static final String DATABASE_NAME = "bookDev";

    public static final String PAGE_POSITION_TABLE = "page_position_table";

    public static final String HIGHLIGHTS_TABLE = "highlights_table";

    public static final String BOOKMARKS_TABLE = "bookmarks_table";

    public static final String FILE_ID = "FILE_ID", PAGE_POSITION = "PAGE_POSITION", FONT_TYPE = "FONT_TYPE", FONT_SIZE = "FONT_SIZE";

    public static final String BOOK_CODE = "BOOK_CODE", CODE = "CODE", CHAPTER_INDEX = "CHAPTER_INDEX", PAGE_POSITION_IN_BOOK = "PAGE_POSITION_IN_BOOK",
            PAGE_POSITION_IN_CHAPTER = "PAGE_POSITION_IN_CHAPTER", START_INDEX = "START_INDEX", END_INDEX = "END_INDEX", START_OFFSET = "START_OFFSET",
            END_OFFSET = "END_OFFSET", COLOR = "COLOR", TEXT_SELECTED = "TEXT_SELECTED", LEFT = "LEFT", TOP = "TOP",
            NOTE = "NOTE", IS_NOTE = "IS_NOTE", IS_OPEN = "IS_OPEN", FOR_SEARCH = "FOR_SEARCH",
            STYLE = "STYLE", PAGE_INDEX = "PAGE_INDEX";

    public static final String NUMBER_OF_CHAPTERS_IN_BOOK = "NUMBER_OF_CHAPTERS_IN_BOOK", NUMBER_OF_PAGES_IN_CHAPTER = "NUMBER_OF_PAGES_IN_CHAPTER", CHAPTER_TITLE = "CHAPTER_TITLE",
            HIGHLIGHTS_IN_PAGE = "HIGHLIGHTS_IN_PAGE", PAGE_DESCRIPTION = "PAGE_DESCRIPTION", DATE_TIME = "DATE_TIME", PAGE_INDEX_IN_BOOK = "PAGE_INDEX_IN_BOOK",
            NUMBER_OF_PAGES_IN_BOOK = "NUMBER_OF_PAGES_IN_BOOK", FIRST_CHARACTER_OFFSET_IN_PAGE = "FIRST_CHARACTER_OFFSET_IN_PAGE", TEXT_LENGTH_IN_PAGE = "TEXT_LENGTH_IN_PAGE";

    public String BOOKMARK_PAGE_INFO = "BOOKMARK_PAGE_INFO";

    public BookDBHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    public BookDBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    public BookDBHelper(Context context, String name, SQLiteDatabase.CursorFactory factory, int version, DatabaseErrorHandler errorHandler) {
        super(context, name, factory, version, errorHandler);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        showLog("table creation", "started");

        db.execSQL("create table " + PAGE_POSITION_TABLE + "(" + FILE_ID + " integer, " + PAGE_POSITION + " real, " +
                "" + FONT_SIZE + " integer, " + FONT_TYPE + " text)");

        db.execSQL("create table " + HIGHLIGHTS_TABLE +
                "(" + BOOK_CODE + " integer, " + CODE + " integer, " + CHAPTER_INDEX + " integer, " + PAGE_POSITION_IN_BOOK + " real, " + PAGE_POSITION_IN_CHAPTER + " real, " +
                "" + START_INDEX + " integer, " + END_INDEX + " integer, " + START_OFFSET + " integer, " + END_OFFSET + " integer, " + COLOR + " integer, " + TEXT_SELECTED + " text, " +
                "" + LEFT + " integer, " + TOP + " integer, " + NOTE + " text, " + IS_NOTE + " text, " + IS_OPEN + " text, " + DATE_TIME + " text, " + FOR_SEARCH + " text, " + STYLE + " integer, " +
                "" + PAGE_INDEX + " integer)");

        db.execSQL("create table " + BOOKMARKS_TABLE +
                "(" + BOOK_CODE + " integer, " + CODE + " integer," + CHAPTER_INDEX + " integer, " + PAGE_INDEX + " integer, " + PAGE_POSITION_IN_BOOK  + " real, " + BOOKMARK_PAGE_INFO + " text)");

        showLog("table creation", "done");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        db.execSQL("DROP TABLE IF EXISTS " + PAGE_POSITION_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + HIGHLIGHTS_TABLE);
        db.execSQL("DROP TABLE IF EXISTS " + BOOKMARKS_TABLE);

        onCreate(db);

    }

    public int getRowCount(String tableName) {

        SQLiteDatabase sqLiteDatabase = getReadableDatabase();

        int numRows = (int) DatabaseUtils.queryNumEntries(sqLiteDatabase, tableName);

        return numRows;
    }

    /**
     * Method to delete all entries from table using DELETE FROM command
     *
     * @param tableName Name of table
     */
    public void truncateTable(String tableName) {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();
        try {
            sqLiteDatabase.execSQL("DELETE FROM " + tableName);
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            sqLiteDatabase.close();
        }

    }

    /**
     * To truncate all tables
     */
    public void truncateAll() {
        truncateTable(PAGE_POSITION_TABLE);
        truncateTable(HIGHLIGHTS_TABLE);
        truncateTable(BOOKMARKS_TABLE);
    }


    //INCLUDE PAGE_POSITION_TABLE
    public boolean isFileIDExists(FileObject fileObject) {
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();

        if (getRowCount(PAGE_POSITION_TABLE) != 0) {

            Cursor cursor = sqLiteDatabase.rawQuery("SELECT * FROM " + PAGE_POSITION_TABLE, null);

            if (cursor.moveToFirst()) {
                do {
                    if (fileObject.getFileID() == cursor.getInt(cursor.getColumnIndex(FILE_ID))) {
                        return true;
                    }
                } while (cursor.moveToNext());
            }

            sqLiteDatabase.close();
        }

        return false;
    }

    public float insertNewIntoPagePositionTable(FileObject fileObject, double pagePosition, int fontSize, String fontType) {

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(FILE_ID, fileObject.getFileID());
        contentValues.put(PAGE_POSITION, pagePosition);
        contentValues.put(FONT_SIZE, fontSize);
        contentValues.put(FONT_TYPE, fontType);

        long rowID = sqLiteDatabase.insert(PAGE_POSITION_TABLE, null, contentValues);

        sqLiteDatabase.close();

        return rowID;
    }

    public int updateIntoPagePositionTable(FileObject fileObject, double pagePosition, int fontSize, String fontType) {

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(PAGE_POSITION, pagePosition);
        contentValues.put(FONT_SIZE, fontSize);
        contentValues.put(FONT_TYPE, fontType);

        return sqLiteDatabase.update(PAGE_POSITION_TABLE, contentValues, FILE_ID + "=?", new String[]{String.valueOf(fileObject.getFileID())});
    }

    public PagePosition readFromPagePositionTable(FileObject fileObject) {
        SQLiteDatabase sqLiteDatabase = getReadableDatabase();
        PagePosition pagePosition = new PagePosition();

        Cursor findEntry = sqLiteDatabase.query(PAGE_POSITION_TABLE, null, FILE_ID + "=?", new String[]{String.valueOf(fileObject.getFileID())}, null, null, null);

        if (findEntry.getCount() != 0) {

            if (findEntry.moveToFirst()) {
                pagePosition.setFileID(findEntry.getInt(findEntry.getColumnIndex(FILE_ID)));
                pagePosition.setPagePosition(findEntry.getDouble(findEntry.getColumnIndex(PAGE_POSITION)));
                pagePosition.setFontSize(findEntry.getInt(findEntry.getColumnIndex(FONT_SIZE)));
                pagePosition.setFontType(findEntry.getString(findEntry.getColumnIndex(FONT_TYPE)));
            }

        }

        findEntry.close();

        sqLiteDatabase.close();

        return pagePosition;
    }

    //END PAGE_POSITION_TABLE

    //INCLUDE HIGHLIGHTS_TABLE
    public float insertNewIntoHighlightsTable(Highlight highlight) {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(BOOK_CODE, highlight.bookCode);
        contentValues.put(CODE, highlight.code);
        contentValues.put(CHAPTER_INDEX, highlight.chapterIndex);
        contentValues.put(PAGE_POSITION_IN_BOOK, highlight.pagePositionInBook);
        contentValues.put(PAGE_POSITION_IN_CHAPTER, highlight.pagePositionInChapter);
        contentValues.put(START_INDEX, highlight.startIndex);
        contentValues.put(END_INDEX, highlight.endIndex);
        contentValues.put(START_OFFSET, highlight.startOffset);
        contentValues.put(END_OFFSET, highlight.endOffset);
        contentValues.put(COLOR, highlight.color);
        contentValues.put(TEXT_SELECTED, highlight.text);
        contentValues.put(LEFT, highlight.left);
        contentValues.put(TOP, highlight.top);
        contentValues.put(NOTE, highlight.note);
        contentValues.put(IS_NOTE, String.valueOf(highlight.isNote));
        contentValues.put(IS_OPEN, String.valueOf(highlight.isOpen));
        contentValues.put(DATE_TIME, String.valueOf(highlight.datetime));
        contentValues.put(FOR_SEARCH, highlight.forSearch);
        contentValues.put(STYLE, highlight.style);
        contentValues.put(PAGE_INDEX, highlight.pageIndex);

        float changedRow = sqLiteDatabase.insert(HIGHLIGHTS_TABLE, null, contentValues);

        sqLiteDatabase.close();

        return changedRow;

    }

    public Highlights getAllHighlights(FileObject fileObject) {

        SQLiteDatabase sqLiteDatabase = getReadableDatabase();

        Highlights highlights = new Highlights();

        Cursor cursor = sqLiteDatabase.query(HIGHLIGHTS_TABLE, null, BOOK_CODE + "=?", new String[]{String.valueOf(fileObject.getFileID())}, null, null, null);

        if (cursor.moveToFirst()) {
            do {

                Highlight highlight = new Highlight();

                highlight.bookCode = cursor.getInt(cursor.getColumnIndex(BOOK_CODE));
                highlight.code = cursor.getInt(cursor.getColumnIndex(CODE));
                highlight.chapterIndex = cursor.getInt(cursor.getColumnIndex(CHAPTER_INDEX));
                highlight.pagePositionInBook = cursor.getDouble(cursor.getColumnIndex(PAGE_POSITION_IN_BOOK));
                highlight.pagePositionInChapter = cursor.getDouble(cursor.getColumnIndex(PAGE_POSITION_IN_CHAPTER));
                highlight.startIndex = cursor.getInt(cursor.getColumnIndex(START_INDEX));
                highlight.endIndex = cursor.getInt(cursor.getColumnIndex(END_INDEX));
                highlight.startOffset = cursor.getInt(cursor.getColumnIndex(START_OFFSET));
                highlight.endOffset = cursor.getInt(cursor.getColumnIndex(END_OFFSET));
                highlight.color = cursor.getInt(cursor.getColumnIndex(COLOR));
                highlight.text = cursor.getString(cursor.getColumnIndex(TEXT_SELECTED));
                highlight.left = cursor.getInt(cursor.getColumnIndex(LEFT));
                highlight.top = cursor.getInt(cursor.getColumnIndex(TOP));
                highlight.note = cursor.getString(cursor.getColumnIndex(NOTE));
                highlight.isNote = Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex(IS_NOTE)));
                highlight.isOpen = Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex(IS_OPEN)));
                highlight.datetime = cursor.getString(cursor.getColumnIndex(DATE_TIME));
                highlight.forSearch = Boolean.parseBoolean(cursor.getString(cursor.getColumnIndex(FOR_SEARCH)));
                highlight.style = cursor.getInt(cursor.getColumnIndex(STYLE));
                highlight.pageIndex = cursor.getInt(cursor.getColumnIndex(PAGE_INDEX));

                highlights.addHighlight(highlight);

            } while (cursor.moveToNext());
        }

        return highlights;
    }

    public int deleteHighlight(Highlight highlight) {

        SQLiteDatabase db = this.getWritableDatabase();

        return db.delete(HIGHLIGHTS_TABLE, CHAPTER_INDEX + " = " + highlight.chapterIndex + " and " + START_INDEX + " = " + highlight.startIndex
                + " and " + END_INDEX + " = " + highlight.endIndex + " and " + START_OFFSET + " = " + highlight.startOffset + " and " + END_OFFSET + " = " + highlight.endOffset, null);
    }

    public void deleteAllHighlights(FileObject fileObject) {

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();

        sqLiteDatabase.execSQL("DELETE FROM " + HIGHLIGHTS_TABLE + " WHERE " + BOOK_CODE + " = " + fileObject.getFileID());
    }

    //END HIGH_LIGHT_TABLE


    //INCLUDE BOOKMARK_TABLE
    public float insertNewIntoBookmarksTable(Bookmark bookmark) {

        SQLiteDatabase sqLiteDatabase = getWritableDatabase();

        ContentValues contentValues = new ContentValues();

        contentValues.put(BOOK_CODE, bookmark.getBookCode());
        contentValues.put(CODE, bookmark.getCode());
        contentValues.put(CHAPTER_INDEX, bookmark.getChapterIndex());
        contentValues.put(PAGE_INDEX, bookmark.getPageIndex());
        contentValues.put(PAGE_POSITION_IN_BOOK, bookmark.getPagePosition());
        contentValues.put(BOOKMARK_PAGE_INFO, bookmark.getBookmarkPageInfo());

        float changedRow = sqLiteDatabase.insert(BOOKMARKS_TABLE, null, contentValues);

        sqLiteDatabase.close();

        return changedRow;
    }

    public int deleteBookmark(Bookmark bookmark) {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();

        return sqLiteDatabase.delete(BOOKMARKS_TABLE, BOOK_CODE + " = " + bookmark.getBookCode() + " and " + CODE + " = " + bookmark.getCode(), null);
    }

    public boolean isBookmarkExists(Bookmark bookmark) {

        SQLiteDatabase sqLiteDatabase = getReadableDatabase();

        Cursor cursor = sqLiteDatabase.query(BOOKMARKS_TABLE, null, BOOK_CODE + "=?", new String[]{String.valueOf(bookmark.getBookCode())}, null, null, null);

        if (cursor.moveToFirst()) {
            do {
                if(cursor.getInt(cursor.getColumnIndex(CODE)) == bookmark.getCode()){
                        return true;
                }
            } while (cursor.moveToNext());
        }

        return false;
    }

    public void toggleBookmark(Bookmark bookmark) {
        if (isBookmarkExists(bookmark)) {
            deleteBookmark(bookmark);
            showLog("bookmark","deleted");
        } else {
            insertNewIntoBookmarksTable(bookmark);
            showLog("bookmark","inserted");
        }
    }

    public ArrayList<Bookmark> getALLBookmarks(FileObject fileObject) {
        SQLiteDatabase sqLiteDatabase = getWritableDatabase();

        Cursor cursor = sqLiteDatabase.query(BOOKMARKS_TABLE, null, BOOK_CODE + "=?", new String[] {String.valueOf(fileObject.getFileID())}, null, null, null);

        ArrayList<Bookmark> bookmarkArrayList = new ArrayList<>();

        if (cursor.moveToFirst()) {
            do {
                Bookmark bookmark = new Bookmark();

                bookmark.setBookCode(cursor.getInt(cursor.getColumnIndex(BOOK_CODE)));
                bookmark.setCode(cursor.getInt(cursor.getColumnIndex(CODE)));
                bookmark.setChapterIndex(cursor.getInt(cursor.getColumnIndex(CHAPTER_INDEX)));
                bookmark.setPageIndex(cursor.getInt(cursor.getColumnIndex(PAGE_INDEX)));
                bookmark.setPagePosition(cursor.getDouble(cursor.getColumnIndex(PAGE_POSITION_IN_BOOK)));
                bookmark.setBookmarkPageInfo(cursor.getString(cursor.getColumnIndex(BOOKMARK_PAGE_INFO)));

                bookmarkArrayList.add(bookmark);

            } while (cursor.moveToNext());
        }

        return bookmarkArrayList;
    }

    //END BOOKMARK_TABLE

    private void showLog(String tag, String value) {
        if (tag.length() > 22) tag = tag.substring(0, 21);
        Log.d(tag, value);
    }
}
