package com.example.abhishek.bookreaderwithskyepub.communicator;

import java.util.List;

/**
 * Created by Abhishek on 10/8/2016.
 */

public interface FragmentCommunicator {

    void sendMessage(String fragmentIdentifier, List contentList);
}
