package com.example.abhishek.bookreaderwithskyepub.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Configuration;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.os.AsyncTask;
import android.os.Build;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.support.annotation.NonNull;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.res.ResourcesCompat;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.abhishek.bookreaderwithskyepub.R;
import com.example.abhishek.bookreaderwithskyepub.communicator.FragmentCommunicator;
import com.example.abhishek.bookreaderwithskyepub.db.BookDBHelper;
import com.example.abhishek.bookreaderwithskyepub.dto.Bookmark;
import com.example.abhishek.bookreaderwithskyepub.dto.FileObject;
import com.example.abhishek.bookreaderwithskyepub.dto.PagePosition;
import com.example.abhishek.bookreaderwithskyepub.utils.Color;
import com.skytree.epub.BookmarkListener;
import com.skytree.epub.Caret;
import com.skytree.epub.ClickListener;
import com.skytree.epub.ContentListener;
import com.skytree.epub.Highlight;
import com.skytree.epub.HighlightListener;
import com.skytree.epub.Highlights;
import com.skytree.epub.MediaOverlayListener;
import com.skytree.epub.NavPoints;
import com.skytree.epub.PageInformation;
import com.skytree.epub.PageMovedListener;
import com.skytree.epub.PageTransition;
import com.skytree.epub.Parallel;
import com.skytree.epub.ReflowableControl;
import com.skytree.epub.SelectionListener;
import com.skytree.epub.SkyProvider;
import com.skytree.epub.State;
import com.skytree.epub.StateListener;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

public class BookReadActivity extends AppCompatActivity implements FragmentCommunicator, View.OnClickListener{

    private ReflowableControl reflowableControl;
    private int fontSize;
    private String fontType;
    private double pagePosition;
    private boolean selectionStartedByUser;
    private boolean showToolbar;

    private SelectionHandler selectionHandler;
    private PageMoveHandler pageMoveHandler;
    private HighlightHandler highlightHandler;
    private StateHandler stateHandler;
    private BookmarkHandler bookmarkHandler;

    private Highlights allHighlights;

    private Highlights notesHighlights;

    private Highlights chapterHighlights;

    private Highlight selectedHighlight;

    private BookDBHelper bookDBHelper;

    private RelativeLayout readerRelativeLayout;

    private ProgressDialog progressDialog;

    private FileObject selectedFileObject;

    private RelativeLayout workSpaceRelativeLayout;

    //toolbar
    private Toolbar toolbar;

    private boolean toolbarMenuOpen;

    private LinearLayout toolbarLinearLayout;

    //Buttons For Fragment Transaction
    private Button tocContentListButton, highlightsListButton, notesListButton, bookmarksListButton;

    //Multiple Button Layout
    private RelativeLayout multipleButtonLayout;

    private ImageButton yellowImageButton, greenImageButton, blueImageButton, textToSpeechImageButton;

    private Button highlightButton, noteButton;

    private RelativeLayout yellowImageButtonRelativeLayout, greenImageButtonRelativeLayout, blueImageButtonRelativeLayout;

    //Note Layout
    private RelativeLayout writeNoteLayout;

    private Button addNoteButton, cancelNoteButton;

    private EditText writeNoteEditText;

    private Bitmap noteIcon;

    //highlight color
    private int highlightColor;

    //Show Highlight Info Layout
    private RelativeLayout showHighLightInfoLayout;

    private ScrollView noteScrollView;

    private TextView noteTextView;

    private Button removeHighLightButton, hideHighlightPopupButton;

    //For Content Listing Layout
    private LinearLayout contentListingLayout;

    private NavPoints tocNavPoints;

    private List tocList, bookmarksList, highlightsList, notesList;

    private RelativeLayout tocListRelativeLayout, highlightsListRelativeLayout, notesListRelativeLayout;

    private boolean tocLoaded;

    private ListView tocContentListView, highlightsListView, notesListView;

    //For Bookmark
    private Bitmap nonBookmarkIconInBitmap, bookmarkedIconInBitmap;

    private Rect bookmarkRect, bookmarkedRect;

    private ArrayList<Bookmark> allBookmarksArrayList;

    private ListView bookmarksListView;

    private RelativeLayout bookmarksListRelativeLayout;

    //For TTS
    //To check RingerMode During Speech
    private int RINGER_MODE;

    private TextToSpeech textToSpeechForTextInHighlight, textToSpeechForTextInPage;

    private String utteranceIdForTextInHighlight = "my_pronounce";

    private String selectedTextInHighlight;

    private String textInPage;

    private String utteranceIdForPage = "my_page_pronounce";

    private boolean pageAutoFlip = false;

    private boolean bookLoadTask = false;

    private MenuItem playPauseMenuItem;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_book_read);

        Intent callingIntent = getIntent();
        selectedFileObject = callingIntent.getParcelableExtra("SELECTED_FILE");

        showLog("file name", selectedFileObject.getFileName());

        if(selectedFileObject != null) {
            initialiseView();
        } else {
            showToast("Not Getting Selected File...");
        }
    }

    private void initialiseView() {
        //BookDBHelper
        bookDBHelper = new BookDBHelper(this);

        //instantiation
        toolbarLinearLayout = (LinearLayout) findViewById(R.id.toolbarLinearLayout);

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(selectedFileObject.getFileName());
        setSupportActionBar(toolbar);

        workSpaceRelativeLayout = (RelativeLayout) findViewById(R.id.workSpaceRelativeLayout);

        readerRelativeLayout = (RelativeLayout) findViewById(R.id.readerRelativeLayout);



        //Multiple button layout
        multipleButtonLayout = (RelativeLayout) findViewById(R.id.multipleButtonLayout);

        yellowImageButton = (ImageButton) findViewById(R.id.yellowImageButton);
        greenImageButton = (ImageButton) findViewById(R.id.greenImageButton);
        blueImageButton = (ImageButton) findViewById(R.id.blueImageButton);
        textToSpeechImageButton = (ImageButton) findViewById(R.id.textToSpeechImageButton);

        highlightButton = (Button) findViewById(R.id.highlightButton);
        noteButton = (Button) findViewById(R.id.noteButton);

        yellowImageButton.setOnClickListener(this);
        greenImageButton.setOnClickListener(this);
        blueImageButton.setOnClickListener(this);
        textToSpeechImageButton.setOnClickListener(this);
        highlightButton.setOnClickListener(this);
        noteButton.setOnClickListener(this);

        yellowImageButtonRelativeLayout = (RelativeLayout) findViewById(R.id.yellowImageButtonRelativeLayout);
        greenImageButtonRelativeLayout = (RelativeLayout) findViewById(R.id.greenImageButtonRelativeLayout);
        blueImageButtonRelativeLayout = (RelativeLayout) findViewById(R.id.blueImageButtonRelativeLayout);

        //Write Note Layout
        writeNoteLayout = (RelativeLayout) findViewById(R.id.writeNoteLayout);

        addNoteButton = (Button) findViewById(R.id.addNoteButton);
        cancelNoteButton = (Button) findViewById(R.id.cancelNoteButton);

        writeNoteEditText = (EditText) findViewById(R.id.writeNoteEditText);


        addNoteButton.setOnClickListener(this);
        cancelNoteButton.setOnClickListener(this);

        //Highlight Info Layout
        showHighLightInfoLayout = (RelativeLayout) findViewById(R.id.showHighLightInfoLayout);

        noteScrollView  = (ScrollView) findViewById(R.id.noteScrollView);
        noteTextView = (TextView) findViewById(R.id.noteTextView);
        removeHighLightButton = (Button) findViewById(R.id.removeHighLightButton);
        hideHighlightPopupButton = (Button) findViewById(R.id.hideHighlightPopupButton);

        removeHighLightButton.setOnClickListener(this);
        hideHighlightPopupButton.setOnClickListener(this);


        //Content Listing Layout
        contentListingLayout = (LinearLayout) findViewById(R.id.contentListingLayout);

        tocListRelativeLayout = (RelativeLayout) findViewById(R.id.tocListRelativeLayout);
        highlightsListRelativeLayout = (RelativeLayout) findViewById(R.id.highlightsListRelativeLayout);
        notesListRelativeLayout = (RelativeLayout) findViewById(R.id.notesListRelativeLayout);
        bookmarksListRelativeLayout = (RelativeLayout) findViewById(R.id.bookmarksListRelativeLayout);

        tocContentListButton = (Button) findViewById(R.id.tocContentListButton);
        highlightsListButton = (Button) findViewById(R.id.highlightsListButton);
        notesListButton = (Button) findViewById(R.id.notesListButton);
        bookmarksListButton = (Button) findViewById(R.id.bookmarksListButton);



        //above four buttons on click listeners
        tocContentListButton.setOnClickListener(this);
        highlightsListButton.setOnClickListener(this);
        notesListButton.setOnClickListener(this);
        bookmarksListButton.setOnClickListener(this);


        tocContentListView = (ListView) findViewById(R.id.tocContentListView);
        highlightsListView = (ListView) findViewById(R.id.highlightsListView);
        notesListView = (ListView) findViewById(R.id.notesListView);
        bookmarksListView = (ListView) findViewById(R.id.bookmarksListView);

        /////////////////////////////////


        progressDialog = new ProgressDialog(this);

        //default values at opening book
        initialiseBookVariableValues(selectedFileObject);

        highlightColor = Color.YELLOW;

        setHighlightColor(highlightColor);

        //TTS
        selectedTextInHighlight = "";

        prepareAndroidTTSForTextInHighlights(selectedTextInHighlight);

        textInPage = "";

        prepareAndroidTTSForTextInPage(textInPage);

        //setting Up ReflowableController and add it to layout
        readerRelativeLayout.addView(setUpReflowableController(selectedFileObject.getFileName(), selectedFileObject.getBaseDirectoryPath()));

        toggleToolBar();

    }

    private void initialiseBookVariableValues(FileObject fileObject) {
        if (bookDBHelper.isFileIDExists(fileObject)) {

            PagePosition pagePosition = bookDBHelper.readFromPagePositionTable(fileObject);

            if (pagePosition != null) {
                this.pagePosition = pagePosition.getPagePosition();
                this.fontSize = pagePosition.getFontSize();
                this.fontType = pagePosition.getFontType();
            } else {
                //This is not possible but just for care
                //default values
                this.pagePosition = 0;
                this.fontSize = 15;
                this.fontType = "TimesRoman";
            }
        } else {
            //default values
            this.pagePosition = 0;
            this.fontSize = 15;
            this.fontType = "TimesRoman";

            //making new entry in pagePosition table
            bookDBHelper.insertNewIntoPagePositionTable(fileObject, this.pagePosition, this.fontSize, this.fontType);
        }

        allHighlights = bookDBHelper.getAllHighlights(selectedFileObject);

        allBookmarksArrayList = bookDBHelper.getALLBookmarks(selectedFileObject);

        loadHighlightsList(allHighlights);

        loadNotesList(allHighlights);

        loadBookmarksList(allBookmarksArrayList);

        chapterHighlights = new Highlights();

        noteIcon = getNoteIconInBitmapForm();

        tocLoaded = false;

        showToolbar = false;

        nonBookmarkIconInBitmap = getNonBookmarkIcon();

        bookmarkedIconInBitmap = getBookmarkIcon();
    }

    private void toggleToolBar() {

        toolbarLinearLayout.setVisibility(View.VISIBLE);

        class ToggleToolBar extends AsyncTask<Void, Void, Void> {

            @Override
            protected Void doInBackground(Void... params) {

                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                super.onPostExecute(aVoid);

                if(toolbarLinearLayout.getVisibility() == View.VISIBLE && toolbarMenuOpen == false) {
                    toolbarLinearLayout.setVisibility(View.INVISIBLE);
                }


            }
        }

        new ToggleToolBar().execute();

    }

    private ReflowableControl setUpReflowableController(String fileName, String baseDirectoryPath) {

        reflowableControl = new ReflowableControl(this);

        fileName = prepareFileNameWithWhiteSpaceReplacement(fileName);

        showLog("fileName", String.valueOf(fileName));

        reflowableControl.setBookName(fileName);

        reflowableControl.setBaseDirectory(baseDirectoryPath);

        reflowableControl.setLicenseKey("0000-0000-0000-0000");

        reflowableControl.setDoublePagedForLandscape(true);

        reflowableControl.setFont(fontType, fontSize);

        reflowableControl.setLineSpacing(135); // the value is supposed to be percent(%).

        reflowableControl.setHorizontalGapRatio(0.25);

        reflowableControl.setVerticalGapRatio(0.1);

        reflowableControl.setPageTransition(PageTransition.Curl);

        reflowableControl.setFingerTractionForSlide(true);

        RelativeLayout.LayoutParams params = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.MATCH_PARENT,
                RelativeLayout.LayoutParams.MATCH_PARENT);

        reflowableControl.setLayoutParams(params);

        SkyProvider skyProvider = new SkyProvider();

        reflowableControl.setContentProvider(skyProvider);

        reflowableControl.setStartPositionInBook(pagePosition);

        reflowableControl.useDOMForHighlight(false);

        reflowableControl.setNavigationAreaWidthRatio(0.0f); // both left and right side.

        //To get text of page in pageMovedListener in pageDescription
        reflowableControl.setExtractText(true);

        reflowableControl.setMediaOverlayListener(new MediaOverlayHandler()); //For Audio Book

        showLog("Media Overlay Available : ", String.valueOf(reflowableControl.isMediaOverlayAvailable()));

        //Listeners
        selectionHandler = new SelectionHandler();
        reflowableControl.setSelectionListener(selectionHandler);

        pageMoveHandler = new PageMoveHandler();
        reflowableControl.setPageMovedListener(pageMoveHandler);

        highlightHandler = new HighlightHandler();
        reflowableControl.setHighlightListener(highlightHandler);

        stateHandler = new StateHandler();
        reflowableControl.setStateListener(stateHandler);

        bookmarkHandler = new BookmarkHandler();
        reflowableControl.setBookmarkListener(bookmarkHandler);

        return reflowableControl;
    }

    @NonNull
    private String prepareFileNameWithWhiteSpaceReplacement(String fileName) {

        return fileName.replaceAll("\\s", "%20");

    }

    private Bitmap getNoteIconInBitmapForm() {
        Drawable markIcon = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_note, null);
        Bitmap iconBitmap = ((BitmapDrawable) markIcon).getBitmap();
        return iconBitmap;
    }

    public Bitmap getNonBookmarkIcon() {
        Drawable markIcon = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_non_bookmark, null);
        Bitmap iconBitmap = ((BitmapDrawable) markIcon).getBitmap();
        return iconBitmap;
    }

    public Bitmap getBookmarkIcon() {
        Drawable markIcon = ResourcesCompat.getDrawable(getResources(), R.drawable.ic_bookmark, null);
        Bitmap iconBitmap = ((BitmapDrawable) markIcon).getBitmap();
        return iconBitmap;
    }

    private void showProgressDialog(String title, String message, boolean cancelable) {
        progressDialog.setTitle(title);
        progressDialog.setMessage(message);
        progressDialog.setCancelable(cancelable);
        progressDialog.show();
    }

    private void hideProgressDialog() {
        if (progressDialog.isShowing()) progressDialog.dismiss();
    }

    private void loadTOCList() {

        tocNavPoints = reflowableControl.getNavPoints();

        tocList = new ArrayList<String>();

        for(int chapterIndex = 0; chapterIndex < tocNavPoints.getSize(); chapterIndex++) {
            tocList.add(tocNavPoints.getNavPoint(chapterIndex).text);
        }

        ArrayAdapter arrayAdapter = new ArrayAdapter(BookReadActivity.this, android.R.layout.simple_list_item_1, tocList);

        tocContentListView.setAdapter(arrayAdapter);

        tocListRelativeLayout.setVisibility(View.VISIBLE);

        tocLoaded = true;

        tocContentListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                reflowableControl.gotoPageByNavPointIndex(position);
                hideContentListingLayout();
            }
        });

    }

    private void loadHighlightsList(final Highlights allHighlights) {

        highlightsList = new ArrayList<String>();

        for (int i = 0; i < allHighlights.getSize(); i++) {
            highlightsList.add(allHighlights.getHighlight(i).text);
        }

        ArrayAdapter arrayAdapter = new ArrayAdapter(BookReadActivity.this, android.R.layout.simple_list_item_1, highlightsList);

        highlightsListView.setAdapter(arrayAdapter);

        highlightsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                reflowableControl.gotoPageByHighlight(allHighlights.getHighlight(position));
                hideContentListingLayout();
            }
        });

    }

    private void loadNotesList(Highlights allHighlights) {

        notesHighlights = new Highlights();

        notesList = new ArrayList<String>();

        for (int i = 0; i < allHighlights.getSize(); i++) {
            if(allHighlights.getHighlight(i).isNote) {
                notesHighlights.addHighlight(allHighlights.getHighlight(i));
                notesList.add(allHighlights.getHighlight(i).note);
            }
        }

        ArrayAdapter arrayAdapter = new ArrayAdapter(BookReadActivity.this, android.R.layout.simple_list_item_1, notesList);

        notesListView.setAdapter(arrayAdapter);

        notesListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                reflowableControl.gotoPageByHighlight(notesHighlights.getHighlight(position));
                hideContentListingLayout();
            }
        });
    }

    private void loadBookmarksList(final ArrayList<Bookmark> allBookmarksArrayList) {

        bookmarksList = new ArrayList<String>();

        for (int i = 0; i < allBookmarksArrayList.size(); i++) {
            bookmarksList.add(allBookmarksArrayList.get(i).getBookmarkPageInfo());
        }

        ArrayAdapter arrayAdapter = new ArrayAdapter(BookReadActivity.this, android.R.layout.simple_list_item_1, bookmarksList);

        bookmarksListView.setAdapter(arrayAdapter);

        bookmarksListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                reflowableControl.gotoPageByPagePositionInBook(allBookmarksArrayList.get(position).getPagePosition());
                hideContentListingLayout();
            }
        });

    }

    private void refreshHighlightList() {

        allHighlights = bookDBHelper.getAllHighlights(selectedFileObject);

        highlightsList = new ArrayList<String>();

        for (int i = 0; i < allHighlights.getSize(); i++) {
            highlightsList.add(allHighlights.getHighlight(i).text);
        }

        ArrayAdapter arrayAdapter = new ArrayAdapter(BookReadActivity.this, android.R.layout.simple_list_item_1, highlightsList);

        highlightsListView.setAdapter(arrayAdapter);

        highlightsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                reflowableControl.gotoPageByHighlight(allHighlights.getHighlight(position));
                hideContentListingLayout();
            }
        });

        loadNotesList(allHighlights);
    }

    private void refreshBookmarkList() {

        allBookmarksArrayList = bookDBHelper.getALLBookmarks(selectedFileObject);

        bookmarksList = new ArrayList<String>();

        for (int i = 0; i < allBookmarksArrayList.size(); i++) {
            bookmarksList.add(allBookmarksArrayList.get(i).getBookmarkPageInfo());
        }

        ArrayAdapter arrayAdapter = new ArrayAdapter(BookReadActivity.this, android.R.layout.simple_list_item_1, bookmarksList);

        bookmarksListView.setAdapter(arrayAdapter);

        bookmarksListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                reflowableControl.gotoPageByPagePositionInBook(allBookmarksArrayList.get(position).getPagePosition());
                hideContentListingLayout();
            }
        });
    }

    private boolean isBookmarkExists(Bookmark bookmark) {

        if (allBookmarksArrayList.size() != 0) {
            for (Bookmark bookmarkTemp : allBookmarksArrayList) {
                if(bookmarkTemp.getCode() == bookmark.getCode())
                    return true;
            }
        }

        return false;
    }

    private void toggleBookmark(Bookmark bookmark) {

        int bookmarkIndex = -1;

        if(isBookmarkExists(bookmark)) {
            for (int i = 0; i < allBookmarksArrayList.size(); i++) {
                if (bookmark.getCode() == allBookmarksArrayList.get(i).getCode()) {
                    bookmarkIndex = i;
                    break;
                }
            }
        }

        if(bookmarkIndex != -1) {
            bookDBHelper.deleteBookmark(bookmark);
        } else {
            bookDBHelper.insertNewIntoBookmarksTable(bookmark);
        }

        refreshBookmarkList();

    }

    private void showToast(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    private void showLog(String tag, String value) {
        if(tag.length() > 22) {
            tag = tag.substring(0, 22);
        }
        Log.d(tag, value);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater menuInflater = getMenuInflater();
        menuInflater.inflate(R.menu.book_read_menu, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.contentListMenu :
                toolbarLinearLayout.setVisibility(View.INVISIBLE);
                showContentListing();
                break;
            case R.id.playBookMenu :

                playPauseMenuItem = item;

                RINGER_MODE = statusRingerMode();

                if(RINGER_MODE == AudioManager.RINGER_MODE_NORMAL)
                    speakStringInPage(textInPage);
                else
                    showToast("Please Remove Silent OR Vibrate Profile Mode");
                break;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    @Override
    protected void onPause() {

        bookDBHelper.updateIntoPagePositionTable(selectedFileObject, pagePosition, fontSize, fontType);

        if(textToSpeechForTextInHighlight != null) {

            if (textToSpeechForTextInHighlight.isSpeaking()) textToSpeechForTextInHighlight.stop();


        }

        if(textToSpeechForTextInPage != null) {

            if (textToSpeechForTextInPage.isSpeaking()) textToSpeechForTextInPage.stop();



        }

        super.onPause();
    }

    @Override
    public void onBackPressed() {

        showLog("onBackPressed","OnBackPressed");

        bookDBHelper.updateIntoPagePositionTable(selectedFileObject, pagePosition, fontSize, fontType);

        if(contentListingLayout.getVisibility() == View.VISIBLE) {
            toggleContentListingLayout();
            showToolbar = false;
        } else if(writeNoteLayout.getVisibility() == View.VISIBLE) {
            toggleWriteNoteLayout();
            showToolbar = false;
        } else if(multipleButtonLayout.getVisibility() == View.VISIBLE) {
            toggleMultipleButtonLayoutVisibility();
            shutDownTTS();
            showToolbar = false;
        } else if(showHighLightInfoLayout.getVisibility() == View.VISIBLE) {
            toggleShowHighlightInfoLayout();
            showToolbar = false;
        } else {
            super.onBackPressed();
            finishActivity();
        }
    }

    @Override
    protected void onDestroy() {

        textToSpeechForTextInHighlight.shutdown();

        textToSpeechForTextInPage.shutdown();

        super.onDestroy();
    }

    @Override
    public void sendMessage(String fragmentIdentifier, List contentList) {

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {
            case R.id.tocContentListButton :
                hideHighlightListRelativeLayout();
                hideNotesListRelativeLayout();
                hideBookmarksListRelativeLayout();
                showTOCListRelativeLayout();
                break;
            case R.id.highlightsListButton :
                hideTOCListRelativeLayout();
                hideNotesListRelativeLayout();
                hideBookmarksListRelativeLayout();
                showHighlightListRelativeLayout();
                break;
            case R.id.notesListButton :
                hideTOCListRelativeLayout();
                hideHighlightListRelativeLayout();
                hideBookmarksListRelativeLayout();
                showNotesListRelativeLayout();
                break;
            case R.id.bookmarksListButton :
                hideTOCListRelativeLayout();
                hideHighlightListRelativeLayout();
                hideNotesListRelativeLayout();
                showBookmarksListRelativeLayout();
                break;
            case R.id.yellowImageButton :
                setHighlightColor(Color.YELLOW);
                break;
            case R.id.greenImageButton :
                setHighlightColor(Color.GREEN);
                break;
            case R.id.blueImageButton :
                setHighlightColor(Color.BLUE);
                break;
            case R.id.highlightButton :
                markHighLight(highlightColor);
                break;
            case R.id.noteButton :
                showWriteNoteLayout();
                break;
            case R.id.textToSpeechImageButton :
                RINGER_MODE = statusRingerMode();

                if(RINGER_MODE == AudioManager.RINGER_MODE_NORMAL)
                    speakStringInHighlight(selectedTextInHighlight);
                else
                    showToast("Please Remove Silent OR Vibrate Profile Mode");
                
                break;
            case R.id.addNoteButton :
                markHighLight(highlightColor, writeNoteEditText.getText().toString());
                break;
            case R.id.cancelNoteButton :
                hideWriteNoteLayout();
                break;
            case R.id.removeHighLightButton :
                deleteHighlight(selectedHighlight);
                break;
            case R.id.hideHighlightPopupButton :
                hideShowHighlightInfoLayout();
                selectedHighlight = null;
                break;
        }

    }

    private void finishActivity() {
        shutDownTTS();
        this.finish();
    }

    //TextToSpeech Operations
    private int statusRingerMode() {
        AudioManager audioManager = (AudioManager) getSystemService(Context.AUDIO_SERVICE);

        if (audioManager.getRingerMode() == AudioManager.RINGER_MODE_SILENT)
            return AudioManager.RINGER_MODE_SILENT;

        if (audioManager.getRingerMode() == AudioManager.RINGER_MODE_VIBRATE)
            return AudioManager.RINGER_MODE_VIBRATE;

        return AudioManager.RINGER_MODE_NORMAL;
    }

    private void prepareAndroidTTSForTextInHighlights(final String text) {
        textToSpeechForTextInHighlight = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                if (status != TextToSpeech.ERROR) {
                    showLog("TTS","Ready To Speak");
                    //just to make performance faster
                    speakStringInHighlight(text);
                } else {
                    showToast("Error While Using TTS Engine");
                }

            }
        });

        textToSpeechForTextInHighlight.setOnUtteranceProgressListener(new UtteranceProgressListener() {
            @Override
            public void onStart(String utteranceId) {
                showLog("onStart", utteranceId);
                setPauseButtonToTextToSpeechImageButton();
            }

            @Override
            public void onDone(String utteranceId) {
                showLog("onDone", utteranceId);
                setPlayButtonToTextToSpeechImageButton();
            }

            @Override
            public void onError(String utteranceId) {
                showLog("onError", "Facing Some Error");
                setPlayButtonToTextToSpeechImageButton();
            }
        });
    }

    private void prepareAndroidTTSForTextInPage(final String textInPage) {
        textToSpeechForTextInPage = new TextToSpeech(this, new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int status) {
                showLog("TTS","Ready To Speak");
                if (status != TextToSpeech.ERROR) {
                    showLog("TTS","Ready To Speak");
                    //just to make performance faster
                    speakStringInPage(textInPage);
                } else {
                    showToast("Error While Using TTS Engine");
                }
            }
        });


        textToSpeechForTextInPage.setOnUtteranceProgressListener(new UtteranceProgressListener() {
            @Override
            public void onStart(String utteranceId) {
                showLog("onStart", utteranceId);
                pageAutoFlip = true;
                setPlayIconToPlayPauseMenuItem();
            }

            @Override
            public void onDone(String utteranceId) {
                showLog("onDone", utteranceId);

                if (pageAutoFlip) {
                    goToNextPageInChapter();
                }

            }

            @Override
            public void onError(String utteranceId) {
                showLog("onError", utteranceId);

            }
        });

    }

    /**
     * This method is used to shut down all the TTS
     */
    private  void shutDownTTS() {
        if(textToSpeechForTextInHighlight != null) {
            textToSpeechForTextInHighlight.stop();
            textToSpeechForTextInHighlight.shutdown();
        }

        if(textToSpeechForTextInPage != null) {
            textToSpeechForTextInPage.stop();
            textToSpeechForTextInPage.shutdown();
        }
    }

    private void speakStringInHighlight(String text) {

        if(textToSpeechForTextInHighlight.isSpeaking()) {
            textToSpeechForTextInHighlight.stop();
            setPlayButtonToTextToSpeechImageButton();
        } else {
            textToSpeechForTextInHighlight.setLanguage(Locale.ENGLISH);
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                textToSpeechForTextInHighlight.speak(text, TextToSpeech.QUEUE_ADD, null, utteranceIdForTextInHighlight);
            } else {
                textToSpeechForTextInHighlight.speak(text, TextToSpeech.QUEUE_ADD, null);
            }
        }
    }
    
    private void speakStringInPage(String textInPage) {

        if (bookLoadTask) {
            if (textToSpeechForTextInPage.isSpeaking()) {
                textToSpeechForTextInPage.stop();
                setPauseIconToPlayPauseMenuItem();
                pageAutoFlip = false;
            } else {
                textToSpeechForTextInPage.setLanguage(Locale.ENGLISH);
                pageAutoFlip = true;
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                    textToSpeechForTextInPage.speak(textInPage, TextToSpeech.QUEUE_ADD, null, utteranceIdForPage);
                } else {
                    textToSpeechForTextInPage.speak(textInPage, TextToSpeech.QUEUE_ADD, null);
                }
            }
        }
    }

    private void setPauseButtonToTextToSpeechImageButton() {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                textToSpeechImageButton.setImageResource(android.R.drawable.ic_media_pause);
            }
        });

    }

    private void setPlayButtonToTextToSpeechImageButton() {

        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                textToSpeechImageButton.setImageResource(android.R.drawable.ic_media_play);
            }
        });
    }

    private void setPlayIconToPlayPauseMenuItem() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(playPauseMenuItem != null) {
                    playPauseMenuItem.setIcon(android.R.drawable.ic_media_play);
                }
            }
        });


    }

    private void setPauseIconToPlayPauseMenuItem() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                if(playPauseMenuItem != null) {
                    playPauseMenuItem.setIcon(android.R.drawable.ic_media_pause);
                }
            }
        });




    }

    private void goToNextPageInChapter() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                reflowableControl.gotoNextPageInChapter();
            }
        });
    }

    //Content Listing Layout Operations
    private void showContentListing() {
        contentListingLayout.setVisibility(View.VISIBLE);
        showTOCListRelativeLayout();
        hideHighlightListRelativeLayout();
        hideNotesListRelativeLayout();
        hideBookmarksListRelativeLayout();
    }

    private void hideContentListingLayout() {
        contentListingLayout.setVisibility(View.INVISIBLE);
    }

    private void toggleContentListingLayout() {
        if(contentListingLayout.getVisibility() == View.VISIBLE) {
            contentListingLayout.setVisibility(View.INVISIBLE);
        } else {
            contentListingLayout.setVisibility(View.VISIBLE);
        }
    }

    private void showTOCListRelativeLayout() { tocListRelativeLayout.setVisibility(View.VISIBLE);}

    private void hideTOCListRelativeLayout() {
        tocListRelativeLayout.setVisibility(View.INVISIBLE);
    }

    private void toggleTOCRelativeLayout() {
        if(tocListRelativeLayout.getVisibility() == View.VISIBLE) {
            tocListRelativeLayout.setVisibility(View.INVISIBLE);
        } else {
            tocListRelativeLayout.setVisibility(View.VISIBLE);
        }
    }

    private void showHighlightListRelativeLayout() {
        highlightsListRelativeLayout.setVisibility(View.VISIBLE);
    }

    private void hideHighlightListRelativeLayout() {
        highlightsListRelativeLayout.setVisibility(View.INVISIBLE);
    }

    private void toggleHighlightListRelativeLayout() {
        if(highlightsListRelativeLayout.getVisibility() == View.VISIBLE) {
            highlightsListRelativeLayout.setVisibility(View.INVISIBLE);
        } else {
            highlightsListRelativeLayout.setVisibility(View.VISIBLE);
        }
    }

    private void showNotesListRelativeLayout() {
        notesListRelativeLayout.setVisibility(View.VISIBLE);
    }

    private void hideNotesListRelativeLayout() {
        notesListRelativeLayout.setVisibility(View.INVISIBLE);
    }

    private void toggleNotesListRelativeLayout() {
        if(notesListRelativeLayout.getVisibility() == View.VISIBLE) {
            notesListRelativeLayout.setVisibility(View.INVISIBLE);
        } else {
            notesListRelativeLayout.setVisibility(View.VISIBLE);
        }
    }

    private void showBookmarksListRelativeLayout() {
        bookmarksListRelativeLayout.setVisibility(View.VISIBLE);
    }

    private void hideBookmarksListRelativeLayout() {
        bookmarksListRelativeLayout.setVisibility(View.INVISIBLE);
    }

    private void toggleBookmarksListRelativeLayout() {
        if (bookmarksListRelativeLayout.getVisibility() == View.VISIBLE) {
            bookmarksListRelativeLayout.setVisibility(View.INVISIBLE);
        } else {
            bookmarksListRelativeLayout.setVisibility(View.VISIBLE);
        }
    }

    //To get pixels
    private int getDensity() {
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int density = metrics.densityDpi;
        return density;
    }

    private float getFactor() {
        return (float) getDensity() / 240.f;
    }

    private int getPX(float dp) {
        return (int) (dp * getFactor());
    }


    //Multiple Button Layout Operations

    /**
     * To toggle multiple button layout
     * */
    private void toggleMultipleButtonLayoutVisibility() {
        if(multipleButtonLayout.getVisibility() == View.VISIBLE) {
            multipleButtonLayout.setVisibility(View.INVISIBLE);
        } else {
            multipleButtonLayout.setVisibility(View.VISIBLE);
        }
    }

    /*
     * To show multiple button layout
     * */
    public void showMultipleButtonLayout() {
        multipleButtonLayout.setVisibility(View.VISIBLE);
    }

    /*
     * To Hide multiple button layout
     * */
    public void hideMultipleButtonLayout() {
        //Hide multiple button layout
        multipleButtonLayout.setVisibility(View.INVISIBLE);

        //Setting Default Positioning For Multiple Button Layout After Hiding
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.leftMargin = 0;
        layoutParams.topMargin = 0;

        multipleButtonLayout.setLayoutParams(layoutParams);

        //To Stop TTS When Multiple Button Hidden
        if(textToSpeechForTextInHighlight.isSpeaking()) {
            textToSpeechForTextInHighlight.stop();
            setPlayButtonToTextToSpeechImageButton();
        }

    }

    /*
    * To move multiple button layout at proper position
    * */
    public void moveMultipleButtonLayoutToUpper(int topMargin, int leftMargin) {
        //Setting Proper Positioning For Multiple Button Layout
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.leftMargin = leftMargin;
        layoutParams.topMargin = topMargin;
        layoutParams.bottomMargin = 30;

        multipleButtonLayout.setLayoutParams(layoutParams);

        showMultipleButtonLayout();
    }

    public void moveMultipleButtonLayoutToBelow(int topMargin, int leftMargin) {

        //Setting Proper Positioning For Multiple Button Layout
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.leftMargin = leftMargin;
        layoutParams.topMargin = topMargin;

        multipleButtonLayout.setLayoutParams(layoutParams);

        showMultipleButtonLayout();
    }

    public void moveMultipleButtonLayoutToMiddle(int topMargin, int leftMargin) {
        //Setting Proper Positioning For Multiple Button Layout
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.leftMargin = leftMargin;
        layoutParams.topMargin = topMargin;

        multipleButtonLayout.setLayoutParams(layoutParams);

        showMultipleButtonLayout();
    }

    /**
     * To delete highlight
     * */
    public void deleteHighlight(Highlight highlight) {
        reflowableControl.deleteHighlight(highlight);
    }

    /*
    * To mark highlight without note
    * */
    public void markHighLight(int color) {
        reflowableControl.markSelection(color, "");
        hideMultipleButtonLayout();
    }

    /*
   * To mark highlight with note
   * */
    public void markHighLight(int color, String note) {
        reflowableControl.markSelection(color, note);
        hideMultipleButtonLayout();
        hideWriteNoteLayout();
    }

    /*
    * To set highlight color selection on multiple button layout
    * */
    private void setHighlightColor(int color) {
        highlightColor = color;
        if(highlightColor == Color.YELLOW) {
            yellowImageButtonRelativeLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.rect_white_border_bg));
            greenImageButtonRelativeLayout.setBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent));
            blueImageButtonRelativeLayout.setBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent));
        } else if(highlightColor == Color.GREEN) {
            yellowImageButtonRelativeLayout.setBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent));
            greenImageButtonRelativeLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.rect_white_border_bg));
            blueImageButtonRelativeLayout.setBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent));
        } else {
            yellowImageButtonRelativeLayout.setBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent));
            greenImageButtonRelativeLayout.setBackgroundColor(ContextCompat.getColor(this, android.R.color.transparent));
            blueImageButtonRelativeLayout.setBackground(ContextCompat.getDrawable(this, R.drawable.rect_white_border_bg));
        }
    }

    /*
    * To show write note layout
    * */
    private void showWriteNoteLayout() {
        writeNoteLayout.setVisibility(View.VISIBLE);
    }

    /*
    * To hide note layout
    * */
    private void hideWriteNoteLayout() {

        //check if keyboard is open
        View focusedView = this.getCurrentFocus();

        if(focusedView != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(focusedView.getWindowToken(), 0);
        }


        writeNoteLayout.setVisibility(View.INVISIBLE);

        writeNoteEditText.setText("");
    }

    /*
    * To toggle note layout
    * */
    private void toggleWriteNoteLayout() {
        if(writeNoteLayout.getVisibility() == View.VISIBLE) {
            //check if keyboard is open
            View focusedView = this.getCurrentFocus();

            if(focusedView != null) {
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(focusedView.getWindowToken(), 0);
            }

            writeNoteLayout.setVisibility(View.INVISIBLE);
        } else {
            writeNoteLayout.setVisibility(View.VISIBLE);
        }
    }


    /*
    * To show Highlight Info Layout
    * */
    private void showHighlightInfoLayout(Highlight highlight) {

        if (highlight.isNote) {
            showNoteInPopUp(highlight.note);
        } else {
            hideNoteInPopUp();
        }


        showHighLightInfoLayout.setVisibility(View.VISIBLE);
    }

    /*
    * To show Note in Show HighLight Layout
    * */
    private void showNoteInPopUp(String note) {
        noteTextView.setText(note);
        noteScrollView.setVisibility(View.VISIBLE);
    }

    /*
    * To hide Note in Show HighLight Layout
    * */
    private void hideNoteInPopUp() {
        noteScrollView.setVisibility(View.GONE);
    }

    /*
    * To Toggle Show Highlight Info Layout
    * */
    private void toggleShowHighlightInfoLayout() {
        if(showHighLightInfoLayout.getVisibility() == View.VISIBLE) {
            showHighLightInfoLayout.setVisibility(View.INVISIBLE);
        } else {
            showHighLightInfoLayout.setVisibility(View.VISIBLE);
        }
    }

    /*
    * To hide Highlight Info Layout
    * */
    private void hideShowHighlightInfoLayout() {
        //Hide multiple button layout
        showHighLightInfoLayout.setVisibility(View.INVISIBLE);

        //Setting Default Positioning For Multiple Button Layout After Hiding
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.leftMargin = 0;
        layoutParams.topMargin = 0;

        showHighLightInfoLayout.setLayoutParams(layoutParams);
        noteTextView.setText("");
        noteScrollView.setVisibility(View.VISIBLE);

    }


    /*
   * To move multiple button layout at proper position
   * */
    public void moveShowHighLightInfoLayoutToUpper(int topMargin, int leftMargin, Highlight highlight) {
        //Setting Proper Positioning For Multiple Button Layout
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.leftMargin = leftMargin;
        layoutParams.topMargin = topMargin;
        layoutParams.bottomMargin = 30;

        showHighLightInfoLayout.setLayoutParams(layoutParams);

        showHighlightInfoLayout(highlight);
    }

    public void moveShowHighLightInfoLayoutToBelow(int topMargin, int leftMargin, Highlight highlight) {

        //Setting Proper Positioning For Multiple Button Layout
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.leftMargin = leftMargin;
        layoutParams.topMargin = topMargin;

        showHighLightInfoLayout.setLayoutParams(layoutParams);

        showHighlightInfoLayout(highlight);
    }

    public void moveShowHighLightInfoLayoutToMiddle(int topMargin, int leftMargin, Highlight highlight) {
        //Setting Proper Positioning For Multiple Button Layout
        RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(
                RelativeLayout.LayoutParams.WRAP_CONTENT,
                RelativeLayout.LayoutParams.WRAP_CONTENT);
        layoutParams.leftMargin = leftMargin;
        layoutParams.topMargin = topMargin;

        showHighLightInfoLayout.setLayoutParams(layoutParams);

        showHighlightInfoLayout(highlight);
    }

    ////////////////////////////////////


    ///////BookMark Implementation////////

    public boolean isTablet() {
        return (getResources().getConfiguration().screenLayout & Configuration.SCREENLAYOUT_SIZE_MASK) >= Configuration.SCREENLAYOUT_SIZE_LARGE;
    }

    public boolean isPortrait() {
        int orientation = getResources().getConfiguration().orientation;
        if (orientation == Configuration.ORIENTATION_PORTRAIT)
            return true;
        else
            return false;
    }

    public int getWidth() {
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        int width = metrics.widthPixels;
        return width;
    }

    public int getPXFromRight(float dip) {
        int ps = this.getPX(dip);
        int ms = this.getWidth() - ps;
        return ms;
    }

    public int getPYFromTop(float dip) {
        int ps = this.getPX(dip);
        return ps;
    }

    public int pxr(float dp) {
        return this.getPXFromRight(dp);
    }

    public int pyt(float dp) {
        return this.getPYFromTop(dp);
    }

    public void calculateFrames() {

        if (!this.isTablet()) { // for phones
            if (this.isPortrait()) {
                int brx = 36 + (44) * 1;
                int bry = 23;
                bookmarkRect = new Rect(pxr(brx), pyt(bry), pxr(brx - 40), pyt(bry + 40));
                bookmarkedRect = new Rect(pxr(brx), pyt(bry), pxr(brx - 38), pyt(bry + 70));
            } else {

                int brx = 40 + (48 + 12) * 1;
                int bry = 14;
                bookmarkRect = new Rect(pxr(brx), pyt(bry), pxr(brx - 40), pyt(bry + 40));
                bookmarkedRect = new Rect(pxr(brx), pyt(bry), pxr(brx - 38), pyt(bry + 70));
            }
        } else { // for tables
            if (this.isPortrait()) {
                int ox = 50;
                int rx = 100;
                int oy = 30;

                int brx = rx - 10 + (44) * 1;
                int bry = oy + 10;
                bookmarkRect = new Rect(pxr(brx), pyt(bry), pxr(brx - 50), pyt(bry + 50));
                bookmarkedRect = new Rect(pxr(brx), pyt(bry), pxr(brx - 50), pyt(bry + 90));
            } else {
                int sd = getPX(40);
                int ox = 40;
                int rx = 130;
                int oy = 20;


                int brx = rx - 20 + (48 + 12) * 1;
                int bry = oy + 10;
                bookmarkRect = new Rect(pxr(brx), pyt(bry), pxr(brx - 40), pyt(bry + 40));
                bookmarkedRect = new Rect(pxr(brx), pyt(bry), pxr(brx - 38), pyt(bry + 70));
            }
        }

    }

    public int getBookmarkCode(int chapterIndex, int pageIndex) {
        return chapterIndex * 1000 + pageIndex;
    }

    /////////////////////////////////////


    ///////////Listeners For Controller

    private class SelectionHandler implements SelectionListener {

        @Override
        public void selectionStarted(Highlight highlight, Rect rect, Rect rect1) {
            showLog("selection", "selection started");
            hideMultipleButtonLayout();
            selectionStartedByUser = true;
            showToolbar = false;
            selectedTextInHighlight = "";
            pageAutoFlip = false;

            if(textToSpeechForTextInHighlight.isSpeaking()) textToSpeechForTextInHighlight.stop();

            if(textToSpeechForTextInPage.isSpeaking()) textToSpeechForTextInPage.stop();

        }

        @Override
        public void selectionChanged(Highlight highlight, Rect rect, Rect rect1) {
            showLog("selection", "selection changed");
            hideMultipleButtonLayout();
            selectionStartedByUser = true;
            showToolbar = false;
            selectedTextInHighlight = "";
        }

        @Override
        public void selectionEnded(Highlight highlight, Rect startRect, Rect endRect) {

            selectionStartedByUser = true;

            showToolbar = false;

            showLog("highlight text", highlight.text);

            selectedTextInHighlight = highlight.text;

            ///////////////////////////////////////////RECT ARGUMENT PROPERTIES////////////////////////////

            showLog("startRect ", "<=RECT Properties=>");

            showLog("startRect top=> ", String.valueOf(startRect.top));
            showLog("startRect bottom=> ", String.valueOf(startRect.bottom));
            showLog("startRect left=> ", String.valueOf(startRect.left));
            showLog("startRect right=> ", String.valueOf(startRect.right));


            ///////////////////////////////////////////RECT1 ARGUMENT PROPERTIES///////////////////////////

            showLog("endRect ", "<=RECT1 Properties=>");

            showLog("endRect top=> ", String.valueOf(endRect.top));
            showLog("endRect bottom=> ", String.valueOf(endRect.bottom));
            showLog("endRect left=> ", String.valueOf(endRect.left));
            showLog("endRect right=> ", String.valueOf(endRect.right));


            ////////////////////////////////////////////EPUB VIEW PROPERTIES///////////////////////////////

            showLog("EPUB view ", "<=EPUB view Properties=>");

            showLog("EPUB view Highlight=> ", String.valueOf(reflowableControl.getHeight()));
            showLog("EPUB view Width=> ", String.valueOf(reflowableControl.getWidth()));
            showLog("EPUB view Top=> ", String.valueOf(reflowableControl.getTop()));
            showLog("EPUB view Bottom=> ", String.valueOf(reflowableControl.getBottom()));
            showLog("EPUB view Left=> ", String.valueOf(reflowableControl.getLeft()));
            showLog("EPUB view Right=> ", String.valueOf(reflowableControl.getRight()));


            ////////////////////////////////////////////MULTIPLE BUTTON LAYOUT PROPERTIES///////////////////

            showLog("Multiple Button Layout ", "<=Multiple Button Layout Properties=>");

            showLog("MBL Height=> ", String.valueOf(multipleButtonLayout.getHeight()));
            showLog("MBL view Width=> ", String.valueOf(multipleButtonLayout.getWidth()));

            ////////////////////////PROBABILITIES OF SELECTION////////////////////////////////////////////////
            ///////////////DON'T MAKE LOGIC CHANGE UNTIL DISCUSS WITH ABHISHEK////////////////////////////////
            if (startRect.top < (reflowableControl.getHeight() / 2) && endRect.bottom < (reflowableControl.getHeight() / 2)) {
                //Now its confirm only part which appears above h/2 is selected
                //Now we have to check whether left of starRect is > than multiple button width
                if ((reflowableControl.getWidth() - startRect.left) > multipleButtonLayout.getWidth()) {
                    moveMultipleButtonLayoutToBelow(endRect.bottom + 30, startRect.left);
                } else {
                    moveMultipleButtonLayoutToBelow(endRect.bottom + 30, reflowableControl.getWidth() - multipleButtonLayout.getWidth());
                }
            } else if (startRect.top > (reflowableControl.getHeight() / 2) && endRect.bottom > (reflowableControl.getHeight() / 2)) {  ///PAGAL ABHYA rect.bottom > (rv.getHeight() / 2) is always true if rect.top > (rv.getHeight() / 2) is true
                //Now we have to check whether left of starRect is > than multiple button width
                if ((reflowableControl.getWidth() - startRect.left) > multipleButtonLayout.getWidth()) {
                    moveMultipleButtonLayoutToUpper(startRect.top - multipleButtonLayout.getHeight() - 30, startRect.left);
                } else {
                    moveMultipleButtonLayoutToUpper(startRect.top - multipleButtonLayout.getHeight() - 30, reflowableControl.getWidth() - multipleButtonLayout.getWidth());
                }
            } else {
                //This vertical center is center of selection not reflowable contrller layout center
                int verticalCenter = startRect.top + (int) ((endRect.bottom - startRect.top) / 2);
                //Now we have to check whether left of starRect is > than multiple button width
                if ((reflowableControl.getWidth() - startRect.left) > multipleButtonLayout.getWidth()) {
                    moveMultipleButtonLayoutToMiddle(verticalCenter, startRect.left);
                } else {
                    moveMultipleButtonLayoutToMiddle(verticalCenter, reflowableControl.getWidth() - multipleButtonLayout.getWidth());
                }
            }
        }

        @Override
        public void selectionCancelled() {
            showLog("Selection", "Canceled");
            hideMultipleButtonLayout();
            hideShowHighlightInfoLayout();
            selectedHighlight = null;
            selectedTextInHighlight = "";
            pageAutoFlip = false;


            if(textToSpeechForTextInHighlight.isSpeaking()) textToSpeechForTextInHighlight.stop();

            if(textToSpeechForTextInPage.isSpeaking()) textToSpeechForTextInPage.stop();

            showLog("showToolbar", String.valueOf(showToolbar));

            if(selectionStartedByUser) {
                selectionStartedByUser = false;
            } else {
                if(showToolbar) {
                    showToolbar = false;
                    toggleToolBar();
                } else {
                    showToolbar = true;
                }
            }

        }
    }

    private class PageMoveHandler implements PageMovedListener {

        @Override
        public void onPageMoved(PageInformation pageInformation) {

            showLog("Page Index", String.valueOf(pageInformation.pageIndex));

            showLog("text", String.valueOf(pageInformation.pageDescription));

            pagePosition = pageInformation.pagePositionInBook;

            textInPage = pageInformation.pageDescription;

            if(textToSpeechForTextInHighlight.isSpeaking()) textToSpeechForTextInHighlight.stop();

            if (textToSpeechForTextInPage.isSpeaking()) textToSpeechForTextInPage.stop();

            if (pageAutoFlip) {
                speakStringInPage(textInPage);
            }


        }

        @Override
        public void onChapterLoaded(int i) {

            showLog("chapter Index", String.valueOf(i));

            if(!tocLoaded) {
                //to load tocListing
                loadTOCList();
            }

            calculateFrames();

        }

        @Override
        public void onFailedToMove(boolean b) {
            showLog("failedToMove", String.valueOf(b));
        }
    }

    private class HighlightHandler implements HighlightListener {

        @Override
        public void onHighlightDeleted(Highlight selectedHighlight) {
            bookDBHelper.deleteHighlight(selectedHighlight);

            refreshHighlightList();

        }

        @Override
        public void onHighlightInserted(Highlight highlight) {

            highlight.bookCode = selectedFileObject.getFileID();

            bookDBHelper.insertNewIntoHighlightsTable(highlight);

            refreshHighlightList();

        }

        @Override
        public void onHighlightUpdated(Highlight highlight) {

        }

        @Override
        public void onHighlightHit(Highlight highlight, int i, int i1, Rect startRect, Rect endRect) {

            selectedHighlight = highlight;

            showLog("On Highlight Hit", "On Highlight Hit");

            ///////////////////////////////////////////RECT ARGUMENT PROPERTIES////////////////////////////

            showLog("startRect ", "<=RECT Properties=>");

            showLog("startRect top=> ", String.valueOf(startRect.top));
            showLog("startRect bottom=> ", String.valueOf(startRect.bottom));
            showLog("startRect left=> ", String.valueOf(startRect.left));
            showLog("startRect right=> ", String.valueOf(startRect.right));


            ///////////////////////////////////////////RECT1 ARGUMENT PROPERTIES///////////////////////////

            showLog("endRect ", "<=RECT1 Properties=>");

            showLog("endRect top=> ", String.valueOf(endRect.top));
            showLog("endRect bottom=> ", String.valueOf(endRect.bottom));
            showLog("endRect left=> ", String.valueOf(endRect.left));
            showLog("endRect right=> ", String.valueOf(endRect.right));


            ////////////////////////////////////////////EPUB VIEW PROPERTIES///////////////////////////////

            showLog("EPUB view ", "<=EPUB view Properties=>");

            showLog("EPUB view Highlight=> ", String.valueOf(reflowableControl.getHeight()));
            showLog("EPUB view Width=> ", String.valueOf(reflowableControl.getWidth()));
            showLog("EPUB view Top=> ", String.valueOf(reflowableControl.getTop()));
            showLog("EPUB view Bottom=> ", String.valueOf(reflowableControl.getBottom()));
            showLog("EPUB view Left=> ", String.valueOf(reflowableControl.getLeft()));
            showLog("EPUB view Right=> ", String.valueOf(reflowableControl.getRight()));

            ////////////////////////////////////////////MULTIPLE BUTTON LAYOUT PROPERTIES///////////////////

            showLog("ShowHighlightInfoLayout", "<=ShowHighlightInfoLayout=>");

            showLog("ShowHighlightInfoLayout Height=> ", String.valueOf(showHighLightInfoLayout.getHeight()));
            showLog("ShowHighlightInfoLayout view Width=> ", String.valueOf(showHighLightInfoLayout.getWidth()));

            ////////////////////////PROBABILITIES OF SELECTION////////////////////////////////////////////////
            ///////////////DON'T MAKE LOGIC CHANGE UNTIL DISCUSS WITH ABHISHEK////////////////////////////////
            if (startRect.top < (reflowableControl.getHeight() / 2) && endRect.bottom < (reflowableControl.getHeight() / 2)) {
                //Now its confirm only part which appears above h/2 is selected
                //Now we have to check whether left of starRect is > than multiple button width
                if ((reflowableControl.getWidth() - startRect.left) > showHighLightInfoLayout.getWidth()) {
                    moveShowHighLightInfoLayoutToBelow(endRect.bottom + 30, startRect.left, highlight);
                } else {
                    moveShowHighLightInfoLayoutToBelow(endRect.bottom + 30, reflowableControl.getWidth() - showHighLightInfoLayout.getWidth(), highlight);
                }
            } else if (startRect.top > (reflowableControl.getHeight() / 2) && endRect.bottom > (reflowableControl.getHeight() / 2)) {  ///PAGAL ABHYA rect.bottom > (rv.getHeight() / 2) is always true if rect.top > (rv.getHeight() / 2) is true
                //Now we have to check whether left of starRect is > than multiple button width
                if ((reflowableControl.getWidth() - startRect.left) > showHighLightInfoLayout.getWidth()) {
                    if(highlight.isNote) {
                        moveShowHighLightInfoLayoutToUpper(startRect.top - showHighLightInfoLayout.getHeight() - 30, startRect.left, highlight);
                    } else {
                        moveShowHighLightInfoLayoutToUpper(startRect.top - showHighLightInfoLayout.getHeight() + noteScrollView.getHeight() - 30, startRect.left, highlight);
                    }
                } else {
                    if(highlight.isNote) {
                        moveShowHighLightInfoLayoutToUpper(startRect.top - showHighLightInfoLayout.getHeight() - 30, reflowableControl.getWidth() - showHighLightInfoLayout.getWidth(), highlight);
                    } else {
                        moveShowHighLightInfoLayoutToUpper(startRect.top - showHighLightInfoLayout.getHeight() + noteScrollView.getHeight() - 30, reflowableControl.getWidth() - showHighLightInfoLayout.getWidth(), highlight);
                    }
                }
            } else {
                //This vertical center is center of selection not reflowable controller layout center
                int verticalCenter = startRect.top + (int) ((endRect.bottom - startRect.top) / 2);
                //Now we have to check whether left of starRect is > than multiple button width
                if ((reflowableControl.getWidth() - startRect.left) > showHighLightInfoLayout.getWidth()) {
                    moveShowHighLightInfoLayoutToMiddle(verticalCenter, startRect.left, highlight);
                } else {
                    moveShowHighLightInfoLayoutToMiddle(verticalCenter, reflowableControl.getWidth() - showHighLightInfoLayout.getWidth(), highlight);
                }
            }




        }

        @Override
        public Highlights getHighlightsForChapter(int chapterIndex) {

            chapterHighlights = new Highlights();

            for (int i = 0; i < allHighlights.getSize(); i++) {
                if(allHighlights.getHighlight(i).chapterIndex == chapterIndex) {
                    chapterHighlights.addHighlight(allHighlights.getHighlight(i));
                }
            }

            return chapterHighlights;
        }

        @Override
        public Bitmap getNoteIconBitmapForColor(int i, int i1) {
            return noteIcon;
        }

        @Override
        public void onNoteIconHit(Highlight highlight) {
            showToast("Note Icon Hit");
        }

        @Override
        public Rect getNoteIconRect(int i, int i1) {

            Rect rect = new Rect(0, 0, getPX(32), getPX(32));

            return rect;
        }

        @Override
        public void onDrawHighlightRect(Canvas canvas, Highlight highlight, Rect rect) {

        }

        @Override
        public void onDrawCaret(Canvas canvas, Caret caret) {

        }
    }

    private class StateHandler implements StateListener {

        @Override
        public void onStateChanged(State state) {
            //States Are NORMAL, LOADING, ROTATING, BUSY
            if (state.equals(State.NORMAL)) {
                showLog("State =>", "NORMAL");
                hideProgressDialog();

                if (!bookLoadTask) bookLoadTask = true;

            } else if (state.equals(State.LOADING)) {
                showLog("State =>", "LOADING");
                showProgressDialog("Loading","Please Wait...",false);
            } else if (state.equals(State.ROTATING)) {
                showLog("State =>", "ROTATING");
                showProgressDialog("Loading","Please Wait...",false);
            } else if (state.equals(State.BUSY)) {
                showLog("State =>", "BUSY");
                showProgressDialog("Loading","Please Wait...",false);
            }
        }
    }

    private class BookmarkHandler implements BookmarkListener {

        @Override
        public void onBookmarkHit(PageInformation pageInformation, boolean b) {
            showLog("Bookmark Hit", "Bookmark Hit");

            Bookmark bookmark = new Bookmark();

            bookmark.setBookCode(selectedFileObject.getFileID());
            bookmark.setCode(getBookmarkCode(pageInformation.chapterIndex, pageInformation.pageIndex));
            bookmark.setChapterIndex(pageInformation.chapterIndex);
            bookmark.setPageIndex(pageInformation.pageIndex);
            bookmark.setBookmarkPageInfo("Chapter : " + pageInformation.chapterIndex + " Page In Chapter : " + pageInformation.pageIndex);
            bookmark.setPagePosition(pageInformation.pagePositionInBook);

            toggleBookmark(bookmark);

            reflowableControl.repaint();
        }

        @Override
        public Rect getBookmarkRect(boolean b) {
            if (b) return bookmarkedRect;
            return bookmarkRect;
        }

        @Override
        public Bitmap getBookmarkBitmap(boolean b) {
            if (b) {
                return bookmarkedIconInBitmap;
            } else
            return nonBookmarkIconInBitmap;
        }

        @Override
        public boolean isBookmarked(PageInformation pageInformation) {

            Bookmark bookmark = new Bookmark();

            bookmark.setBookCode(selectedFileObject.getFileID());
            bookmark.setCode(getBookmarkCode(pageInformation.chapterIndex, pageInformation.pageIndex));

            return isBookmarkExists(bookmark);
        }
    }

    private class ContentHandler implements ContentListener {
        @Override
        public long getLength(String s, String s1) {
            return 0;
        }

        @Override
        public InputStream getInputStream(String s, String s1) {
            return null;
        }

        @Override
        public boolean isExists(String s, String s1) {
            return false;
        }

        @Override
        public long getLastModified(String s, String s1) {
            return 0;
        }
    }

    private class MediaOverlayHandler implements MediaOverlayListener {
        @Override
        public void onParallelStarted(Parallel parallel) {
            showLog("onParallelStarted","onParallelStarted");
        }

        @Override
        public void onParallelEnded(Parallel parallel) {
            showLog("onParallelEnded With Arg","onParallelEnded With Arg");
        }

        @Override
        public void onParallelsEnded() {
            showLog("onParallelStarted","onParallelStarted");
        }
    }

    private class ClickHandler implements ClickListener {

        @Override
        public void onClick(int i, int i1) {

        }

        @Override
        public void onImageClicked(int i, int i1, String s) {

        }

        @Override
        public void onLinkClicked(int i, int i1, String s) {

        }

        @Override
        public void onLinkForLinearNoClicked(int i, int i1, String s) {

        }

        @Override
        public boolean ignoreLink(int i, int i1, String s) {
            return false;
        }

        @Override
        public void onIFrameClicked(int i, int i1, String s) {

        }

        @Override
        public void onVideoClicked(int i, int i1, String s) {

        }

        @Override
        public void onAudioClicked(int i, int i1, String s) {

        }
    }

}
